from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Receipt, Account
from django.contrib.auth.decorators import login_required
from django.conf import settings
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipts_list(request):
    receipts_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts_list": receipts_list}
    return render(request, "receipts/receipts_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form, "page": "Create Receipt"}
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category_list}
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {"account_list": account_list}
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form, "page": "Create Category"}
    return render(request, "receipts/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form, "page": "Create Account"}
    return render(request, "receipts/create.html", context)
